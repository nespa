#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>  // Header File For The OpenGL32 Library
#include <OpenGL/glu.h> // Header File For The GLu32 Library
#else
#include <GL/gl.h>  // Header File For The OpenGL32 Library
#include <GL/glu.h> // Header File For The GLu32 Library
#endif
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_timer.h"
#include "SDL/SDL_mixer.h"

#include "e3d_engine.h"



SDL_Surface *LoadBMP(char *filename)
{
    Uint8 *rowhi, *rowlo;
    Uint8 *tmpbuf, tmpch;
    SDL_Surface *image;
    int i, j;

    image = SDL_LoadBMP(filename);
    if ( image == NULL ) {
        fprintf(stderr, "Unable to load %s: %s\n", filename, SDL_GetError());
        return(NULL);
    }

    /* GL surfaces are upsidedown and RGB, not BGR :-) */
    tmpbuf = (Uint8 *)malloc(image->pitch);
    if ( tmpbuf == NULL ) {
        fprintf(stderr, "Out of memory\n");
        return(NULL);
    }


    rowhi = (Uint8 *)image->pixels;
    rowlo = rowhi + (image->h * image->pitch) - image->pitch;

    for ( i=0; i<image->h/2; ++i ) {
        for ( j=0; j<image->w; ++j ) {
            tmpch = rowhi[j*3];
            rowhi[j*3] = rowhi[j*3+2];
            rowhi[j*3+2] = tmpch;
            tmpch = rowlo[j*3];
            rowlo[j*3] = rowlo[j*3+2];
            rowlo[j*3+2] = tmpch;
        }
        memcpy(tmpbuf, rowhi, image->pitch);
        memcpy(rowhi, rowlo, image->pitch);
        memcpy(rowlo, tmpbuf, image->pitch);
        rowhi += image->pitch;
        rowlo -= image->pitch;
    }
    free(tmpbuf);
    return(image);
}



e3D_Application::~e3D_Application()
{
  if (Video) delete Video;
  if (Sound) delete Sound;
  if (Controller) delete Controller;
  if (Render) delete Render;
}

e3D_Application::e3D_Application()
{
  printf("e3D_Application ");
  Render=new e3D_Render(this);
  printf(".. R ");
  Video=new e3D_Video(this);
  printf(".. V ");
  Sound=new e3D_Sound(this);
  printf(".. S ");
  Controller=new e3D_Controller(this);
  printf(".. C ");
  time_start=SDL_GetTicks();
  printf("... constructed\n");
}

// ********************************************

int e3D_Video::InitializeVideo()
{
  if (!Initialize()) return 0;
  if (VideoInit) return 1;
  Uint32 flags = SDL_OPENGL;
  if (Fullscreen) flags = flags | SDL_FULLSCREEN;
  if ( SDL_SetVideoMode(Width,Height, Bitsperpixel, flags) == NULL ) {
      fprintf(stderr, "Unable to create OpenGL screen: %s\n", SDL_GetError());
      return 0;
  }
  if (__E3D_ENGINE_DEBUG) printf("Video Mode Changed\n");
  glViewport(0, 0, Width,Height);
  if (__E3D_ENGINE_DEBUG) printf("GL Viewport Resized\n");
  SDL_WM_SetCaption(APPLICATION_TITLE, NULL);
  if (__E3D_ENGINE_DEBUG) printf("App Caption Changed\n");
  VideoInit=1;
  if (App->Render) App->Render->Init(Width,Height);
  return 1;
}




void e3D_Render::Init(int Width,int Height)
{
	cout << "e3D_Render: Init" << endl;
	aspect_ratio=(double)Width/(double)Height;

	OG_Stars=new e3D_Object(World);
	OG_Planets=new e3D_Object(World);
	OG_Ships=new e3D_Object(World);
	OG_Particles=new e3D_Object(World);
	OG_HUD=new e3D_Object(World);

	Time=new e3D_Text(OG_HUD);
	Score=new e3D_Text(OG_HUD);
	Speed=new e3D_Text(OG_HUD);
	Fuel=new e3D_Text(OG_HUD);

	glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(30.0f,aspect_ratio,0.1f,10000000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_LIGHT0);
	float WHITE[4]={1,1,1,1};
	float GREY[4]={0.5,0.5,0.5,1};
	float BLACK[4]={0.2,0.2,0.2,1};
	glLightfv(GL_LIGHT0,GL_AMBIENT,BLACK);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,GREY);
	glLightfv(GL_LIGHT0,GL_SPECULAR,WHITE);
	e3D_Checkpoint *Checkpoint;

//	Ship1=new e3D_Ship(OG_Ships,"data/ships/n13.obj",0.50);
	Ship1=new e3D_Ship(OG_Ships,"data/ships/n13.obj",0.50);

	Ship2=new e3D_Ship(OG_Ships,"data/ships/n1.obj",0.50);

	Ship3=new e3D_Ship(OG_Ships,"data/ships/n1b.obj",0.50);

	Camera=Ship1;

	int i;
	char file[100];

	float size=0;
	float x=1;
	float y=1;
	float z=1;
	float d;
	int b;
	float ddd=50000.0;
	for (i=0;i<10;i++)
	{
		d=sqrt(x*x+y*y);
		d=sqrt(d*d+z*z)/(ddd);
		ddd/=1.5;
		for (b=0;b<50;b++)
		{
			size=rand()%10000/100.0+5500;
			if (b) sprintf(file,"images/estrella.png");
			else
			{
				sprintf(file,"images/particles/brightstar-256-128.png");

			}
			Planet1=new e3D_Sprite(OG_Stars,file);

			Planet1->Translate(x+(rand()%400-200)*size,y+(rand()%400-200)*size,z+(rand()%400-200)*size);
			Planet1->Scale(size,size,size);
			Planet1->Scale((11-i)/15.0,(11-i)/15.0,(11-i)/15.0);
		}


		size=(rand()%10000/200000.0)*ddd+ddd/150.0;

		x+=x/d+(rand()%10000-5000)/10000.0*ddd;
		y+=y/d+(rand()%10000-5000)/10000.0*ddd;
		z+=z/d+(rand()%10000-5000)/10000.0*ddd;
		int nfile=rand()%3+1;

		if (i==0)
		{
			size=rand()%10000/2000.0+50;
			sprintf(file,"images/sun.png");
			Sun=new e3D_Sprite(OG_Planets,file,size*size);
			Sun->geom=dCreateSphere(space,size*size*.05);
			dGeomSetBody(Sun->geom,Sun->body);
			dMassSetSphere(&(Sun->mass),25000,size*size*0.05);

		}
		else
		{
			sprintf(file,"images/planeta%d.png",nfile);
			Planet1=new e3D_Sprite(OG_Planets,file,size/2.0);
			Planet1->geom=dCreateSphere(space,size/2.0*0.75);
			dGeomSetBody(Planet1->geom,Planet1->body);
			dMassSetSphere(&(Planet1->mass),15000,size/2.0*0.75);
			Planet1->Translate(x,y,z);
			Planet1->shadowed=1;
			Checkpoint=new e3D_Checkpoint(Planet1);
		}
	}

	sprintf(file,"images/estrella.png");
	Planet1=new e3D_Sprite(World,file);

	Planet1->Translate(x*8,y*8,z*8);
	Planet1->Scale(size*size*50,size*size*50,size*size*50);


	sprintf(file,"images/particles/hiddenplanet-256-128.png");
	Planet1=new e3D_Sprite(World,file);

	Planet1->Translate(-x*20,-y*20,-z*20);
	Planet1->Scale(size*size*10,size*size*10,size*size*10);


	Blackhole=new e3D_Sprite(World,file);

	Blackhole->Translate(x*8,y*8,z*8);
	Blackhole->Scale(size*size*8,size*size*8,size*size*8);

	x*=1.015;
	y*=1.015;
	z*=1.015;
	OG_Planets->children.reverse();
	Ship1->Translate(x+0,y+100,z+100);
	Ship2->Translate(x-100,y+0,z+0);
	Ship3->Translate(x-100,y-100,z+0);




}











// ******************** Auxiliary functions.


void matrixidentity(matriz M)
{
  int i;
  for (i=0;i<16;i++)
  {
    M[i]=0;
    if (i%5==0) M[i]=1;
  }

}

void matrixresetrot(matriz M)
{
  int i,x,y;
  double sx,sy,sz;
  sx=sqrt(M[0]*M[0]+M[1]*M[1]);
  sx=sqrt(sx*sx+M[2]*M[2]);

  sy=sqrt(M[4]*M[4]+M[5]*M[5]);
  sy=sqrt(sy*sy+M[6]*M[6]);

  sz=sqrt(M[8]*M[8]+M[9]*M[9]);
  sz=sqrt(sz*sz+M[10]*M[10]);

  for (i=0;i<16;i++)
  {
    x=i%4;
    y=(i-x)/4;
    y++;x++;
    if (x<=3 && y<=3)
    {
      M[i]=0;
      if (i%5==0) M[i]=1;
    }
  }
  M[0]=sx;
  M[5]=sy;
  M[10]=sz;

}

void matrix2angle(matriz M, double *x, double *y,double *z)
{
  *x=M[0]+M[1]+M[2]+M[3];
  *y=M[4]+M[5]+M[6]+M[7];
  *z=M[8]+M[9]+M[10]+M[11];
//  *w=M[12]+M[13]+M[14]+M[15];
}

void matrix2point(matriz M, double *x, double *y,double *z,double px,double py,double pz)
{
//  double x1,y1,z1;
//  matrix2angle(M, &x1,&y1,&z1);

  *x=M[0]*px+M[4]*py+M[8] *pz+M[12];
  *y=M[1]*px+M[5]*py+M[9] *pz+M[13];
  *z=M[2]*px+M[6]*py+M[10]*pz+M[14];

/*  *x=M[12];
  *y=M[13];
  *z=M[14];*/

//  *w=M[3]+M[7]+M[11]+M[15];

}

