#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>	// Header File For The OpenGL32 Library
#include <OpenGL/glu.h>	// Header File For The GLu32 Library
#else
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library
#endif
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_timer.h"
#include "SDL/SDL_mixer.h"
#include "SDL_Pango.h"


#include "ModelType.h"
#include "e3d_engine.h"

#define NUM_PLANETS 3
#define NUM_SHOOTS 3
#define MAX_SHOTS 512

// 640x480 * 2 * 2 = 1280 x 960

SDL_Surface *pixshoot[NUM_SHOOTS];

e3D_Application *e3D_App;
e3D_Render *Render;
e3D_Video *Video;
e3D_Sound *Sound;
e3D_Controller *Controller;

Mix_Music *music_intro;
Mix_Music *music_introgame;
Mix_Music *music_game;

uint sound_checkpoint1,
	sound_checkpoint2a,
	sound_checkpoint2b,
	sound_checkpoint2c,
	sound_destroyed,
	sound_engine1,
	sound_planet1a,
	sound_shoot1dd,
	sound_shoot1d,
	sound_shoot1f,
	sound_shoot1,
	sound_shoot2;

int channel_engine;

GLuint    texplanet[NUM_PLANETS];        // Storage For NUM_PLANETS Textures
GLuint    texshoot[NUM_SHOOTS];        // Storage For NUM_SHOOTS Textures

GLuint    cube;            // Storage For The Display List
//GLuint    top;            // Storage For The Second Display List
GLuint    sprite;

GLuint    xloop;            // Loop For X Axis
GLuint    yloop;            // Loop For Y Axis
float zoom=1;

GLfloat    xrot;            // Rotates Cube On The X Axis
GLfloat    yrot;            // Rotates Cube On The Y Axis




matriz camera_position, camera_orientation, camera, perspective;
matriz ship;
double sz;
ModelType MTObject[15];

// Build Cube Display List
GLvoid BuildList()
{
    sprite=glGenLists(1);
    glNewList(sprite,GL_COMPILE);
        glBegin(GL_QUADS);
            // Top Face
            glNormal3f( 0.0f, 0.0f, -1.0f);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f, 0.0f);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f, 0.0f);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  -1.0f,  0.0f);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  -1.0f,  0.0f);
        glEnd();
    glEndList();
}

// Load Bitmaps And Convert To Textures
void LoadGLTextures(void)
{
    // Load Texture
    int i;


    i=0;

    pixshoot[i] = IMG_Load("images/particles/supernova-256-8.png");
    if (!pixshoot[i]) {SDL_Quit();exit(1);}
    i++;
    pixshoot[i] = IMG_Load("images/particles/cross1.png");
    if (!pixshoot[i]) {SDL_Quit();exit(1);}
    i++;
    pixshoot[i] = IMG_Load("images/particles/cross3.png");
    if (!pixshoot[i]) {SDL_Quit();exit(1);}
    i++;

/*		SDLPango_Context *pango_test1 = SDLPango_CreateContext();
		SDLPango_SetDefaultColor(pango_test1, MATRIX_TRANSPARENT_BACK_WHITE_LETTER);
		SDLPango_SetDpi(pango_test1, TTF_DPI, TTF_DPI);
    SDLPango_SetMinimumSize(pango_test1, -1, 0);
		SDLPango_SetMarkup(pango_test1, "world", -1);
    int ps_w = SDLPango_GetLayoutWidth(pango_test1);
    int ps_h = SDLPango_GetLayoutHeight(pango_test1);
		// int margin_x = 10;
		// int margin_y = 10;
		//		pixshoot[2]= SDLPango_CreateSurfaceDraw(pango_test1);
		pixshoot[2] = SDL_CreateRGBSurface(SDL_SWSURFACE, ps_w, ps_h,
				32, (Uint32)(255 << (8 * 0)), (Uint32)(255 << (8 * 1)),
				(Uint32)(255 << (8 * 2)), (Uint32)(255 << (8 * 3)));
		SDLPango_Draw(pango_test1, pixshoot[2], 0, 0);
		//int totalbytes=4*pixshoot[2]->w*pixshoot[2]->h;
*/

    // Create MipMapped Texture
    glGenTextures(NUM_SHOOTS, texshoot);

    for (i=0;i<NUM_SHOOTS;i++)
    {
      glBindTexture(GL_TEXTURE_2D, texshoot[i]);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
      gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB,
        pixshoot[i]->w, pixshoot[i]->h, GL_RGB, GL_UNSIGNED_BYTE,
        pixshoot[i]->pixels);
    }

};

/* A general OpenGL initialization function.  Sets all of the initial parameters. */
void TempInitGL()            // We call this right after our OpenGL window is created.
{
    LoadGLTextures();                             // Load The Texture
    BuildList();                                // Build The Display List
}

/* The main drawing function. */
int puntos=0;

void DrawTitles()
{
	glMatrixMode(GL_MODELVIEW);
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
	glDepthMask(GL_FALSE);

	glBlendFunc(GL_SRC_ALPHA,GL_ONE); // Aditivo

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                            // Reset The Projection Matrix
	gluOrtho2D(-1,1,-1,1);
	glMatrixMode(GL_MODELVIEW);

	glDisable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glLoadIdentity();
}

void DrawGLScene(float d=0.03, float angle=0, float vel=0)
{
	static int frame=0;

		float a=10/Render->Ship1->camlag;
		if (a>2) a=2;

    glMatrixMode(GL_MODELVIEW);


    glDisable(GL_LIGHTING);
    glEnable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
    glDepthMask(GL_FALSE);

    glBlendFunc(GL_SRC_ALPHA,GL_ONE); // Aditivo

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                            // Reset The Projection Matrix
    gluOrtho2D(-1,1,-1,1);
    glMatrixMode(GL_MODELVIEW);

    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

		frame++;
		static float aux_score=0;
		aux_score=(aux_score*100+Render->Ship1->score)/101.0;
		float score_sz=10+sin(e3D_App->total_time/10000.0+aux_score/10.0);
		Render->Score->setScale(score_sz,score_sz);

		if (frame%6==0)
		{
				Render->Time->A=a;
				Render->Score->A=a;
				Render->Speed->A=a;
				Render->Fuel->A=a;

			if (puntos==0)
			{
				int minX=300,minY=0;
				float scaleX=2,scaleY=2;
				Render->Time->setAttrs(minX,minY,scaleX,scaleY);
				Render->Score->setAttrs(minX,minY,scaleX,scaleY);
				Render->Speed->setAttrs(minX,minY,scaleX,scaleY);
				Render->Fuel->setAttrs(minX,minY,scaleX,scaleY);
				Render->Time->x=-0.50;
				Render->Time->y=-0.90;
				Render->Score->x=0.8;
				Render->Score->y=0.8;
				Render->Speed->x=0.1;
				Render->Speed->y=-0.9;
				Render->Fuel->x=0.8;
				Render->Fuel->y=-0.9;

			}


			char ff[]="font_family=\"TSCu_Comic\"";
			char txt[256];
			puntos++;

			sprintf(txt,"<span %s>Score:%06d</span>",ff,Render->Ship1->score);
			Render->Score->setText(txt);

			sprintf(txt,"<span %s>Time:%.2f</span>",ff,(e3D_App->total_time-Render->Ship1->time_start)/1000.0);
			Render->Time->setText(txt);

			sprintf(txt,"<span %s>Speed:%.2f</span>",ff,Render->Ship1->lv);
			Render->Speed->setText(txt);

			sprintf(txt,"<span %s>Fuel:%.2f</span>",ff,Render->Ship1->used_fuel);
			Render->Fuel->setText(txt);

			frame=0;
		}

		glLoadIdentity();

		Render->Score->Draw(1);
		Render->Time->Draw(1);
		Render->Speed->Draw(1);
		Render->Fuel->Draw(1);

		a/=2.0;
    glBindTexture(GL_TEXTURE_2D, texshoot[2]);

    glLoadIdentity();
    glScalef(d/(4.0/3.0),d,1.0f);
    //glScalef(5,5,1.0f);
    glRotatef(angle,0,0,1);
    glColor4f(1.0f,1.0f,1.0f,(0.05f/d-0.05)*a);
    glCallList(sprite);

    glBindTexture(GL_TEXTURE_2D, texshoot[2]);
    static float va=0;
    va+=sqrt(vel)*2.0;
    glLoadIdentity();
    glTranslatef(0.8+1/vel,-0.8-1/vel,0);
    glScalef(3.0/4.0/vel,1.0f/vel,1.0f);
    glRotatef(va,0,0,1);
    glColor4f(1.0f,1.0f,1.0f,(vel/10.0)*a);
    glCallList(sprite);


}


void nearCallback (void *data, dGeomID o0, dGeomID o1) {
	e3D_Render *Render=(e3D_Render *)data;
	// Create an array of dContact objects to hold the contact joints
	static const int MAX_CONTACTS = 10;
	dContact contact[MAX_CONTACTS];

	for (int i = 0; i < MAX_CONTACTS; i++)
	{
		contact[i].surface.mode = dContactSoftERP | dContactSoftCFM;
		contact[i].surface.mu = dInfinity;
		contact[i].surface.mu2 = 0;
		contact[i].surface.bounce = 0.0;
		contact[i].surface.bounce_vel = 0.0;
		contact[i].surface.soft_cfm = 0.1;
		contact[i].surface.soft_erp = 0.1;
	}
	if (int numc = dCollide(o0, o1, MAX_CONTACTS, &contact[0].geom, sizeof(dContact)))
	{
		Sound->play_sound(sound_shoot1f,.4);
		// Get the dynamics body for each geom
		dBodyID b1 = dGeomGetBody(o0);
		dBodyID b2 = dGeomGetBody(o1);
		// To add each contact point found to our joint group we call dJointCreateContact which is just one of the many
		// different joint types available.
		for (int i = 0; i < numc; i++)
		{
			// dJointCreateContact needs to know which world and joint group to work with as well as the dContact
			// object itself. It returns a new dJointID which we then use with dJointAttach to finally create the
			// temporary contact joint between the two geom bodies.
			dJointID c = dJointCreateContact(Render->world, Render->_contactgroup, contact + i);
			dJointAttach(c, b1, b2);
		}
	}
}


void callback_Checkpoint_onChange(int new_checkpoint)
{
	Sound->play_sound(sound_checkpoint1);
	return;
	switch(rand()%3)
	{
		case 0:	Sound->play_sound(sound_checkpoint2a); break;
		case 1:	Sound->play_sound(sound_checkpoint2b); break;
		case 2:	Sound->play_sound(sound_checkpoint2c); break;
	}
}

void callback_Checkpoint_done(int checkpoint, int points_earned)
{

}

void hook_musicDone() {
		Sound->play_music(music_game,1);
}

#ifdef WIN32
int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine,
                    int iCmdShow)
#endif
#ifndef WIN32
int main(int argc, char **argv)
#endif
{

    int done;
    Uint8 *keys;
    float zoom_d=1;
    Uint32 start;


    e3D_App=new e3D_Application();
    Sound=e3D_App->Sound;
    Video=e3D_App->Video;
    Controller=e3D_App->Controller;
    Render=e3D_App->Render;

    Video->Width=1024;
    Video->Height=768;
    Video->Bitsperpixel=0;
    Video->Fullscreen=0;

    Video->InitializeVideo();
    Controller->SetWarpPos(Video->Width/2,Video->Height/2);

    e3D_App->SetFPS(60);

		sound_checkpoint1=Sound->load_sound("sounds/checkpoint1.ogg");
		sound_checkpoint2a=Sound->load_sound("sounds/checkpoint2a.ogg");
		sound_checkpoint2b=Sound->load_sound("sounds/checkpoint2b.ogg");
		sound_checkpoint2c=Sound->load_sound("sounds/checkpoint2c.ogg");
		sound_destroyed=Sound->load_sound("sounds/destroyed.ogg");
		sound_engine1=Sound->load_sound("sounds/engine1.ogg");
		sound_planet1a=Sound->load_sound("sounds/planet1a.ogg");
		sound_shoot1dd=Sound->load_sound("sounds/shoot1-dd.ogg");
		sound_shoot1d=Sound->load_sound("sounds/shoot1-d.ogg");
		sound_shoot1f=Sound->load_sound("sounds/shoot1-f.ogg");
		sound_shoot1=Sound->load_sound("sounds/shoot1.ogg");
		sound_shoot2=Sound->load_sound("sounds/shoot2.ogg");

    SDL_Joystick *joystick=0;
    int njj=SDL_NumJoysticks();
    if (njj >= 1)
    {
        cout << "Se encontraron " << njj << " joysticks." << endl;
        joystick = SDL_JoystickOpen(0);
    }

    TempInitGL();

    done = 0;
    start=SDL_GetTicks();
    e3D_Ship *Nave=Render->Ship1;
//    e3D_Ship *Nave2=Render->Ship2;
//    e3D_Ship *Nave3=Render->Ship3;

    dWorldID world = Render->world;
    #define MUS_CYRIL "music/CyrilPereira-TheRevengeOfGlory/"
  	music_intro=Mix_LoadMUS(MUS_CYRIL "18 - blacksheep alien.ogg");
  	music_introgame=Mix_LoadMUS(MUS_CYRIL "01 - Le cercle maudit.ogg");
  	music_game=Mix_LoadMUS(MUS_CYRIL "02 - The Revenge Of Glory.ogg");

    int weapon1=0;
    /*
    double joyx=0,joycx=0;
    double joyy=0,joycy=0;
    double joyz=0,joycz=0;
    */
		e3D_Ship *eCamera=new e3D_Ship(Render->OG_Ships,"data/ships/n2.obj",2.50);

    // e3D_Object *eCamera=new e3D_Object(Render->World);
		e3D_Text *text_nespa= new e3D_Text(Render->OG_HUD);
		e3D_Text *text_createdby= new e3D_Text(Render->OG_HUD);
		e3D_Text *text_hitesc= new e3D_Text(Render->OG_HUD);
		//char ff[]="font_family=\"TSCu_Comic\"";
		text_createdby->setAttrs(0,0,2,2);
		text_createdby->setText("<span font_family=\"TSCu_Comic\">A Game Created By: David Martinez Marti</span>");
		//text_createdby->setText("A Game Created By David Martínez Martí");
		text_createdby->y=0.6;
		//text_createdby->setScale(0,0);
		text_createdby->A=0.0;

		text_nespa->setAttrs(0,0,20,20);
		text_nespa->setText("<span face=\"Radis Sans\" color=\"red\">nespa</span>\n<span face=\"Radis Sans\" size=\"3000\" gravity=\"east\"> a starship game</span>");
		text_nespa->y=0.4;
		text_nespa->setScale(0,0);
		text_nespa->A=0.3;

		text_hitesc->setAttrs(0,0,2,2);
		text_hitesc->setText("<span face=\"Radis Sans\">Hit <i>[ESC]</i> to Skip Intro</span>");
		text_hitesc->y=0.4;
		text_hitesc->x=-0.4;
		text_hitesc->A=0;

		Render->Camera=eCamera;
//		Render->Camera=Nave;
		eCamera->Translate(0,0,2000);
		uint time_start=e3D_App->total_time;
		Controller->UnGrab();
		zoom_d=Render->zoom=0.4;

		Sound->play_music(music_intro,1);
		Mix_HookMusicFinished(hook_musicDone);

		dBodyAddRelForceAtRelPos(eCamera->body,20,20,20,0.001,0.001,-0.01);
		dBodyDisable(Nave->body);

    cout << "Iniciando juego." << endl;
		while (!done)
		{
        Render->zoom=(Render->zoom*100+zoom_d)/101.0;
        if (e3D_App->StartFrame())
        {

          //if (Controller->grab)
          {
          	double Cx,Cy,Cz;
          	double Sx,Sy,Sz;
          	eCamera->GetXYZ(&Cx,&Cy,&Cz);
          	Nave->GetXYZ(&Sx,&Sy,&Sz);
						float time=(e3D_App->total_time-time_start)/1000.0;
						if (time<5)
						{
							dBodyAddRelForceAtRelPos(eCamera->body,2.3,0,90,0,0,-0.01);
							zoom_d=2.5;
							text_nespa->y=(text_nespa->y*100+0.4)/101.0;
							text_nespa->x/=1.01;

						} else if (time<9)
						{
							static float accel=0.0008;
							text_nespa->setScale(1000/accel,1000/accel);

							if (accel<100) accel*=1.017;
							zoom_d=2.0;
						} else if (time<40)
						{
							if (time<11) zoom_d=1; else zoom_d+=.01;
							if (text_nespa->y>-0.6) text_nespa->y-=0.001; else
							{
								text_nespa->x=(text_nespa->x*1000.0+0.6)/1001.0;
								text_nespa->scaleX=(text_nespa->scaleX*1000.0+70)/1001.0;
								text_nespa->scaleY=(text_nespa->scaleY*1000.0+70)/1001.0;

								if (text_createdby->A<0.7) text_createdby->A+=0.001;
								else {
									text_createdby->scaleX*=1.01;
									if (text_createdby->scaleX>1000) text_createdby->y+=.003;
								}

							}

							const dReal *AngularVel, *LinearVel;

							AngularVel=dBodyGetAngularVel(eCamera->body);
							LinearVel=dBodyGetLinearVel(eCamera->body);

							double AngularVel_x=AngularVel[0],AngularVel_y=AngularVel[1],AngularVel_z=AngularVel[2];
							double LinearVel_x=LinearVel[0],LinearVel_y=LinearVel[1],LinearVel_z=LinearVel[2];

							// Fricción simulada / irreal:
							double ax=(Sx-Cx),ay=(Sy-Cy),az=(Sz-Cz);
							double dxy=sqrt(ax*ax+ay*ay);
							double d=sqrt(dxy*dxy+az*az);
							double d2=sqrt(d)*1.2;
							if (d2<0.5) d2=0.5;

							double avd=1;
							dBodyAddTorque(eCamera->body,-AngularVel_x*avd,-AngularVel_y*avd,-AngularVel_z*avd);
							double lvd=1;
							if (time>15.0) {lvd=0;d2/=5.0;}
							if (time>24 && zoom_d>8) {zoom_d-=0.1; lvd=0.5;}
							if (time>29 && zoom_d>2) {zoom_d-=0.02; lvd=2;}
							if (time>33 && zoom_d>1) {zoom_d-=0.1; lvd=5;}
							dBodyAddForce(eCamera->body,-LinearVel_x*lvd,-LinearVel_y*lvd,-LinearVel_z*lvd);

							// printf("%.2f | %.2f\n",d,d2);
							dBodyAddForceAtRelPos(eCamera->body,ax/d2,ay/d2,az/d2,0,0,-0.01);

							dBodyAddForceAtRelPos(Nave->body,-Sx/1000000.0,-Sy/1000000.0,-Sz/1000000.0,0,0,-100);
						} else if (time<80)
						{
							const dReal *AngularVel, *LinearVel;
							text_hitesc->A=(text_hitesc->A*100+sin(time*10.0)+.5)/101.0;

							AngularVel=dBodyGetAngularVel(eCamera->body);
							LinearVel=dBodyGetLinearVel(eCamera->body);

							double AngularVel_x=AngularVel[0],AngularVel_y=AngularVel[1],AngularVel_z=AngularVel[2];
							double LinearVel_x=LinearVel[0],LinearVel_y=LinearVel[1],LinearVel_z=LinearVel[2];

							// Fricción simulada / irreal:
							double ax=(-Cx),ay=(-Cy),az=(-Cz);
							double dxy=sqrt(ax*ax+ay*ay);
							double d=sqrt(dxy*dxy+az*az);
							double d2=sqrt(d)*3;
							if (d2<0.5) d2=0.5;

							double avd=0.01;
							dBodyAddTorque(eCamera->body,-AngularVel_x*avd,-AngularVel_y*avd,-AngularVel_z*avd);
							double lvd=0.1;
							dBodyAddForce(eCamera->body,-LinearVel_x*lvd,-LinearVel_y*lvd,-LinearVel_z*lvd);

							// printf("%.2f | %.2f\n",d,d2);
							dBodyAddForceAtRelPos(eCamera->body,ax/d2,ay/d2,az/d2,0,0,-0.00001);


						} else time_start=e3D_App->total_time;

						dWorldStep(world,1.0/60.0);

            glLoadIdentity();
          	//DrawGLScene(1,1,1);
            Render->Render();
            DrawTitles();
            text_nespa->Draw(0);
            text_createdby->Draw(0);
            text_hitesc->Draw(0);
          }
          SDL_GL_SwapBuffers();
        }
        done=Controller->GetEvents(); // OJO!! Esto asigna *keys.

        keys = Controller->keys;
        if ( keys[SDLK_F7] == SDL_PRESSED ) Controller->Grab();
        if ( keys[SDLK_F8] == SDL_PRESSED ) Controller->UnGrab();
        if ( keys[SDLK_F1] == SDL_PRESSED ) Render->Camera=eCamera;
        if ( keys[SDLK_F2] == SDL_PRESSED ) Render->Camera=Nave;
		}

		done=false;
		channel_engine=Sound->play_sound(sound_engine1,0,-1);
 		Render->Camera=Render->Ship1;
 		Controller->Grab();
 		zoom_d=1;
		dBodyEnable(Nave->body);
 		dBodySetForce(Nave->body, 0,0,0);
		dBodySetTorque(Nave->body, -12,20,0);
		dWorldStep(world,.3);
		dBodySetTorque(Nave->body, 0,0,0);
		dBodySetAngularVel (Nave->body,0,0,0);
		dWorldStep(world,.3);
		Nave->camlag=100;

		Sound->play_music(music_introgame,1);
		Mix_HookMusicFinished(hook_musicDone);
    while (!done)
    {
        if (e3D_App->StartFrame())
        {

          if (Controller->grab)
          {
				 		if (Nave->camlag>1) Nave->camlag-=.1;

						Nave->dfriction=5.0/(Nave->lv+1000)+.02;
						dSpaceCollide(Render->space, Render, &nearCallback);
						dWorldStep(world,1.0/60.0);
						dJointGroupEmpty(Render->_contactgroup);

            double x2,y2,z2;
            double x3,y3,z3;
            glLoadIdentity();
//            Render->LoadCamera(Render->Ship1);
//            Render->Ship2->GetXYZ(&x2,&y2,&z2);
//            Render->Ship3->GetXYZ(&x3,&y3,&z3);

            // Draw only when StartFrame returns True
            Render->Render();
            float e1=Nave->energy;
            if (e1>1000) e1=1000;
            float d=10.0/e1*zoom;
            if (d>0.5) d=0.5;
            if (d<0.02) d=0.02;
            //float a=1;
            // Indicadores varios y disparos
            DrawGLScene(d,-weapon1*90/5,fabs(Nave->dz));
						Mix_Volume(channel_engine,MIX_MAX_VOLUME/10.0*sqrt(fabs(Nave->dz)));

/*
						// GUIAS DE JOYSTICK
            glBindTexture(GL_TEXTURE_2D, texshoot[0]);
            glLoadIdentity();
            glScalef(0.1/(4.0/3.0),0.1,1.0f);
            glTranslated(joyx*fabs(joyx)/10.0,joyy*fabs(joyy)/10.0,0);
            glColor4f(1.0f,0.0f,0.0f,1.0f);
            glCallList(sprite);
            glLoadIdentity();
            glScalef(0.1/(4.0/3.0),0.1,1.0f);
            glTranslated(-joycx*5,-joycy*5,0);
            glColor4f(0.0f,0.0f,1.0f,1.0f);
            glCallList(sprite);
*/
						// El HUD / radar que dibuja a los enemigos.
/*            glBindTexture(GL_TEXTURE_2D, texshoot[1]);
            if (z2<-500 && Render->Ship2->hull)
            {
              x2/=-z2/zoom/3.8;
              y2/=-z2/zoom/3.8;
              glLoadIdentity();
              glScalef(1.0/(4.0/3.0),1,1.0f);
              glTranslated(x2,y2,0);
              d=1.8/sqrt(-z2+500)*sqrt(zoom);

              a=d/0.1;
              if (a>1) a=1;
              if (d>0.5) d=0.5;
              if (d<0.02) d=0.02;

              glScalef(d,d,1.0f);
              glColor4f(1.0f,1.0f,1.0f,(0.05f/d-0.05)*a);
              glCallList(sprite);
            }
            if (z3<-500 && Render->Ship3->hull)
            {
              x3/=-z3/zoom/3.8;
              y3/=-z3/zoom/3.8;
              glLoadIdentity();
              glScalef(1.0/(4.0/3.0),1,1.0f);
              glTranslated(x3,y3,0);
              d=1.8/sqrt(-z3+500)*sqrt(zoom);

              a=d/0.1;
              if (a>1) a=1;
              if (d>0.5) d=0.5;
              if (d<0.02) d=0.02;

              glScalef(d,d,1.0f);
              glColor4f(1.0f,1.0f,1.0f,(0.05f/d-0.05)*a);
              glCallList(sprite);
            }
*/
          }
          SDL_GL_SwapBuffers();
        }
        else
        {
					Mix_Volume(channel_engine,0);
        }
        done=Controller->GetEvents(); // OJO!! Esto asigna *keys.

				Nave->ry=Controller->mouse_x/20.0/zoom;
				Nave->rx=Controller->mouse_y/20.0/zoom;

				// Suavizado del zoom.
        zoom=(zoom_d+zoom*50.0)/51.0;

        Render->zoom=zoom;
        sz=Nave->energy;

        /* Check current key state for special commands */
        keys = Controller->keys;

        if ( keys[SDLK_a] == SDL_PRESSED ) Nave->rz-= 0.2f;
        if ( keys[SDLK_s] == SDL_PRESSED ) Nave->rz+= 0.2f;

        if ( keys[SDLK_z] == SDL_PRESSED ) {Nave->dz+=.5;}
        if ( keys[SDLK_x] == SDL_PRESSED ) {Nave->dz-=1;}
        if ( keys[SDLK_SPACE] == SDL_PRESSED && Nave->dz>0 && Nave->dz<100) {Nave->dz*=1.1;}
        if (weapon1<60) weapon1++;
        if (weapon1>=30)
        {

        	if (keys[SDLK_v] == SDL_PRESSED)
        	{
						weapon1=0;
						Sound->play_sound(sound_shoot1,.2);
						Nave->Shoot(0);
        	}
        	if (keys[SDLK_b] == SDL_PRESSED)
        	{
						weapon1=0;
						Sound->play_sound(sound_shoot1,.2);
						Nave->Shoot(1);
        	}
        }

/*
				// Sobre música y gramola
        if ( keys[SDLK_1] == SDL_PRESSED && playing>=0 )
        {
            if (Mix_FadeOutMusic(10000))            playing=-1;
        }

        if (playing==-1 || !Mix_PlayingMusic())
        {
          if (Mix_FadingMusic()==MIX_NO_FADING)
          {
            playing=rand()%11;
            Mix_FadeInMusicPos(music[playing],1,10000,0);

          }
        }
        */

				// Teclas de función
        if ( keys[SDLK_F1] == SDL_PRESSED ) Render->Camera=Render->Ship1;
        if ( keys[SDLK_F2] == SDL_PRESSED ) Render->Camera=Render->Ship2;
        if ( keys[SDLK_F3] == SDL_PRESSED ) Render->Camera=Render->Ship3;

        if ( keys[SDLK_F7] == SDL_PRESSED ) Controller->Grab();
        if ( keys[SDLK_F8] == SDL_PRESSED ) Controller->UnGrab();


				// Freno

        if ( keys[SDLK_LCTRL])
        {
          Nave->dz/=1.10;
          Nave->dx/=1.10;
          Nave->dy/=1.10;
        }

				// Zoom
        if ( Controller->mouse_wheel>0 && zoom_d < 100) zoom_d*=1.5;
        if ( Controller->mouse_wheel<0 && zoom_d > 0.5) zoom_d/=1.5;
        if (zoom_d > 100) zoom_d=100;
        if (zoom_d < 0.5) zoom_d=0.5;

// ********************************************
    }

    if (joystick && SDL_JoystickOpened(0))
    {
        SDL_JoystickClose(joystick);
    }
    delete e3D_App;
    return 0;
}

