#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>  // Header File For The OpenGL32 Library
#include <OpenGL/glu.h> // Header File For The GLu32 Library
#else
#include <GL/gl.h>  // Header File For The OpenGL32 Library
#include <GL/glu.h> // Header File For The GLu32 Library
#endif
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_timer.h"
#include "SDL/SDL_mixer.h"

#include "e3d_engine.h"

// Desde nespa.cpp; Callbacks
extern void callback_Checkpoint_onChange(int new_checkpoint);
extern void callback_Checkpoint_done(int checkpoint, int points_earned);


string e3D_Sprite::spritelist[MAX_SPRITE_LIMIT];
GLuint e3D_Sprite::maxsprite;
GLuint e3D_Sprite::sprite_shadow;
e3D_Checkpoint e3D_Checkpoint::*checkpointlist[128];
int e3D_Checkpoint::maxcheckpoint=0;
int e3D_Checkpoint::currentpoint=0;


e3D_Sprite::e3D_Sprite(e3D_Object *parent, const char *sprite) : e3D_Object(parent)
{
//    Ship.LoadObj(fileobj,size);
//    cout << "Create ship:" << fileobj << endl;
  //matrixidentity(position);
  //matrixidentity(inverse);
  //matrixidentity(inverse_orientation);
  GLuint i;
  nsprite=0;
  for (i=1;i<=this->maxsprite && i<MAX_SPRITE_LIMIT;i++)
  {
     if (this->spritelist[i]==sprite)
     {
        nsprite=i;
    //    cout << "Located sprite " << sprite << " onto " << nsprite << endl;
     }
  }
  if (!nsprite) LoadSprite(sprite);
  angle=0;
  shadowed=0;
  R=G=B=A=1;

  init_physics();
}

e3D_Sprite::e3D_Sprite(e3D_Object *parent, const char *sprite, double size, double density) : e3D_Object(parent)
{
  GLuint i;
  nsprite=0;
  shadowed=0;
  for (i=1;i<=this->maxsprite && i<MAX_SPRITE_LIMIT;i++)
  {
     if (this->spritelist[i]==sprite)
     {
        nsprite=i;
     }
  }
  if (!nsprite) LoadSprite(sprite);
  angle=0;
  R=G=B=A=1;
  init_physics(size,density);
}

void e3D_Sprite::LoadShadow(const char *spritefile)
{
    SDL_Surface *pixsprite;
    pixsprite = IMG_Load(spritefile);
    if (!pixsprite) {
     cout << "Can't load sprite " << spritefile << " onto " << nsprite << endl;
    return;}

    // Create MipMapped Texture
    GLuint texture;
    glGenTextures(1, &texture);
    if (texture)
    {
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
      gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA,
        pixsprite->w, pixsprite->h, GL_RGBA, GL_UNSIGNED_BYTE,
        pixsprite->pixels);

    }
    sprite_shadow=texture;
    spritelist[sprite_shadow]=spritefile;
    if (maxsprite<sprite_shadow) maxsprite=sprite_shadow;

    cout << "Loaded sprite " << spritefile << " onto " << texture << endl;

}


void e3D_Sprite::LoadSprite(const char *spritefile)
{
		if (!this->sprite_shadow)
		{
			LoadShadow("images/shadow1.png");
		}
    SDL_Surface *pixsprite;
    pixsprite = IMG_Load(spritefile);
    if (!pixsprite) {
     cout << "Can't load sprite " << spritefile << " onto " << nsprite << endl;
    return;}

    // Create MipMapped Texture
    GLuint texture;
    glGenTextures(1, &texture);
    if (texture)
    {
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
      gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA,
        pixsprite->w, pixsprite->h, GL_RGBA, GL_UNSIGNED_BYTE,
        pixsprite->pixels);

    }
    nsprite=texture;
    spritelist[nsprite]=spritefile;
    if (maxsprite<nsprite) maxsprite=nsprite;
    if (__E3D_ENGINE_DEBUG) cout << "Loaded sprite " << spritefile << " onto " << nsprite << endl;

}

void e3D_Sprite::Draw(int pass=0)
{

  glDisable(GL_LIGHTING);
  glEnable(GL_BLEND);
  //glDepthMask(GL_FALSE);
//  glDepthMask(GL_TRUE);

  glPushMatrix();

  matriz M;
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  matrixresetrot(M);
  glLoadMatrixd(M);
  glRotated(angle,0,0,1);
  if (shadowed)
  {
  glBlendFunc(GL_ONE_MINUS_SRC_ALPHA,GL_SRC_COLOR); // Sustractivo
  glBindTexture(GL_TEXTURE_2D, sprite_shadow);
  glColor4f(1.0f,1.0f,1.0f,1.0f);
  glBegin(GL_QUADS);
      glNormal3f( 0.0f, 0.0f, -1.0f);
      glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
      glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  -1.0f, 0.0f);
  glEnd();
  }

  glBindTexture(GL_TEXTURE_2D, nsprite);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE); // Aditivo
  //Render->LoadCameraRot(Render->Camera);
  glColor4f(R,G,B,A);
  glBegin(GL_QUADS);
      glNormal3f( 0.0f, 0.0f, -1.0f);
      glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
      glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  -1.0f, 0.0f);
  glEnd();


  glPopMatrix();
}


void e3D_Shoot::Draw(int pass=0)
{
	if (life<0) return;
	ApplyGravityForces(Render->OG_Planets);
	if (scale_x<max_size)
	{
		scale_x*=1+0.02/scale_x;
		scale_y*=1+0.02/scale_y;
		scale_z*=1+0.02/scale_z;
		dBodyAddRelForce(body,0,0,-speed*(max_size-scale_x));
	}
	life-=1.0/60.0;
	if (life<0)
	{
		AutoDeleteAfterDraw=1;
	}
	else if (life<3)
	{
		scale_x=max_size*(life)/3.0;
		scale_y=max_size*(life)/3.0;
		scale_z=max_size*(life)/3.0;
	}
  glBindTexture(GL_TEXTURE_2D, nsprite);
  glDisable(GL_LIGHTING);
  glEnable(GL_BLEND);
  glDepthMask(GL_FALSE);
  glDisable(GL_ALPHA_TEST);

  glBlendFunc(GL_SRC_ALPHA,GL_ONE); // Aditivo

  glPushMatrix();

  matriz M;
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  matrixresetrot(M);
  glLoadMatrixd(M);
  glRotated(angle,0,0,1);
  //Render->LoadCameraRot(Render->Camera);
  //glColor4f(1.0f,1.0f,1.0f,1.0f);
  glColor4f(R,G,B,A);
  glBegin(GL_QUADS);
      glNormal3f( 0.0f, 0.0f, -1.0f);
      glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
      glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  -1.0f, 0.0f);
  glEnd();
  glPopMatrix();
}


void e3D_Checkpoint::Draw(int pass)
{
	e3D_Sprite::Draw(pass);
	float d=1+sin(Render->App->total_time/120.0)/15.0;
	if (cpoint!=currentpoint) d=1+sin(Render->App->total_time/240.0)/15.0;
	glScaled(d,d,d);
	e3D_Sprite::Draw(pass);

	double Ax,Ay,Az;
	double Bx,By,Bz;
	Parent->GetXYZ(&Ax,&Ay,&Az);
	Render->Ship1->GetXYZ(&Bx,&By,&Bz);

	double ABx=Bx-Ax,ABy=By-Ay,ABz=Bz-Az;
	double Rxy=sqrt(ABx*ABx+ABy*ABy);
	double R2=Rxy*Rxy+ABz*ABz;
	double R=sqrt(R2);
	if (cpoint!=currentpoint && cpoint!=currentpoint-1)
	{
		this->A=(this->A*800+0)/801.0;
		this->R=(this->R*30+1)/31.0;
		this->G=(this->G*60+1)/61.0;
		this->B=(this->B*120+1)/121.0;
	}
	else
	{
		this->A=(this->A*300+1)/301.0;
		if (cpoint==currentpoint)
		{
			if (cpoint==0)
			{
				this->R=(this->R*100+0)/101.0;
				this->G=(this->G*100+1)/101.0;
				this->B=(this->B*100+0.5)/101.0;
			}
			else
			{
				this->R=(this->R*100+1)/101.0;
				this->G=(this->G*100+2/scale_x)/101.0;
				this->B=(this->B*100+1/scale_x)/101.0;
			}
		}
		else
		{
			this->R=(this->R*100+0)/101.0;
			this->G=(this->G*30+.2)/31.0;
			this->B=(this->B*100+1)/101.0;
		}
	}
	if (this->R>1) this->R=1;
	if (this->G>1) this->G=1;
	if (this->B>1) this->B=1;
	if (this->A>1) this->A=1;
	R-=8.0;
	if (R<0.1) R=0.1;
	if (R/planet_size<scale_x) {
		if (cpoint==currentpoint)
		{
			currentpoint--;
			callback_Checkpoint_onChange(currentpoint);
		}
		if (cpoint>currentpoint)
		{
			int score=sqrt(planet_size)*50.0*(scale_x-R/planet_size)/(R/planet_size);
			total_score+=score;
			Render->Ship1->score+=score;
			if (Render->Ship1->used_fuel<0) Render->Ship1->used_fuel=0;
			scale_x=scale_y=scale_z=R/planet_size;
		}
	} else if (R/planet_size>scale_x*3 && scale_x<2 && cpoint>currentpoint) {

		if (total_score)
		{
			callback_Checkpoint_done(cpoint, total_score);
			total_score=0;
		}
		scale_x+=0.001;
		scale_y+=0.001;
		scale_z+=0.001;
	}
}

e3D_Checkpoint::e3D_Checkpoint(e3D_Object *parent) : e3D_Sprite(parent,"images/escudos.png")
{
	R=1.0;
	G=0.2;
	B=0.0;
	A=0.0;
	total_score=0;
	planet_size=Parent->scale_x;
	float max_size=sqrt(planet_size)*1;
	Scale(max_size,max_size,max_size);
	//checkpointlist[maxcheckpoint]=this;
	cpoint=currentpoint=maxcheckpoint;
	maxcheckpoint++;
}
