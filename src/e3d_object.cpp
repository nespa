#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>  // Header File For The OpenGL32 Library
#include <OpenGL/glu.h> // Header File For The GLu32 Library
#else
#include <GL/gl.h>  // Header File For The OpenGL32 Library
#include <GL/glu.h> // Header File For The GLu32 Library
#endif
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_timer.h"
#include "SDL/SDL_mixer.h"

#include "e3d_engine.h"


e3D_Object::e3D_Object(e3D_Render *render)
{
  Parent=NULL;
  Render=render;
  body=0;
  geom=0;

  ObjectClassname="Object";
  // future classes should be named like:
  // Object.Planet
  // Object.Ship
  // Object.Particle
  // Object.Particle.Atmosphere
  // A distinctive ObjectClassname is needed when trying to guess the pass number in the render.
	init_physics();
  camera_eyeX=0;
  camera_eyeY=0;
  camera_eyeZ=0;

  camera_centerX=0;
  camera_centerY=0;
  camera_centerZ=100;

  camera_upX=0;
  camera_upY=10;
  camera_upZ=0;
	camlag=100;

}

e3D_Object::e3D_Object(e3D_Object *parent)
{
  if (parent)
  {
  //  cout << "Child added to parent" << endl;
    e3D_Object(parent->Render);
    Parent=parent;
    Parent->children.insert(Parent->children.begin(),this);
    Render=Parent->Render;
  }
  ObjectClassname="Object";
	init_physics();
  camera_eyeX=0;
  camera_eyeY=0;
  camera_eyeZ=0;

  camera_centerX=0;
  camera_centerY=0;
  camera_centerZ=100;

  camera_upX=0;
  camera_upY=10;
  camera_upZ=0;
	camlag=100;
}
/*
void e3D_Object::init_physics()
{
//	printf("init_physics\n");
	body = dBodyCreate(Render->world);


	dMassSetSphere(&mass,1,1);
	dBodySetMass(body,&mass);
	dBodySetPosition(body,0,0,0);
	scale_x=scale_y=scale_z=1;
	AutoDeleteAfterDraw=0;
}
*/
void e3D_Object::init_physics(double size,double density)
{
//	printf("init_physics\n");
	body = dBodyCreate(Render->world);
	camlag=100;
	dMass mass;
	dMassSetSphere(&mass,density,size);
	dBodySetMass(body,&mass);
	dBodySetPosition(body,0,0,0);
	scale_x=scale_y=scale_z=size;
	AutoDeleteAfterDraw=0;
}

void e3D_Object::Free()
{
  if (Parent)
  {
    Parent->children.remove(this);
    Parent=0;
  }
  if (body)
  {
    dBodyDestroy (body);
  }
  if (geom)
  {
    dGeomDestroy (geom);
  }
}

e3D_Object::~e3D_Object()
{
	Free();
}

/*
void e3D_Object::LoadPosition() {glLoadMatrixd(position);}
void e3D_Object::MultPosition() {glMultMatrixd(position);}
void e3D_Object::SavePosition() {glGetDoublev(GL_MODELVIEW_MATRIX,position);}

void e3D_Object::LoadInverse() {glLoadMatrixd(inverse);}
void e3D_Object::MultInverse() {glMultMatrixd(inverse);}
void e3D_Object::SaveInverse() {glGetDoublev(GL_MODELVIEW_MATRIX,inverse);}

void e3D_Object::LoadInverseO() {glLoadMatrixd(inverse_orientation);}
void e3D_Object::MultInverseO() {glMultMatrixd(inverse_orientation);}
void e3D_Object::SaveInverseO() {glGetDoublev(GL_MODELVIEW_MATRIX,inverse_orientation);}
*/
void e3D_Object::ResetAtBody(dBodyID dest,double x, double y, double z, double vel_mult)
{
	dVector3 p;
	dBodyGetRelPointPos(dest,x,y,z,p);
	const dReal *r = dBodyGetRotation(dest);
	const dReal *AngularVel, *LinearVel;
	AngularVel=dBodyGetAngularVel(dest);
	LinearVel=dBodyGetLinearVel(dest);

	dBodySetLinearVel(body,LinearVel[0]*vel_mult,LinearVel[1]*vel_mult,LinearVel[2]*vel_mult);
	dBodySetPosition(body,p[0],p[1],p[2]);
	dBodySetRotation(body,r);
}

void e3D_Object::LoadRealPosition()
{

	const dReal *p = dBodyGetPosition(body);
	const dReal *r = dBodyGetRotation(body);

	double m[16];

	m[ 0] = r[ 0];m[ 1] = r[ 4];m[ 2] = r[ 8];m[ 3] = 0;
	m[ 4] = r[ 1];m[ 5] = r[ 5];m[ 6] = r[ 9];m[ 7] = 0;
	m[ 8] = r[ 2];m[ 9] = r[ 6];m[10] = r[10];m[11] = 0;
	m[12] = p[ 0];m[13] = p[ 1];m[14] = p[ 2];m[15] = 1;
	glMultMatrixd(m);
	glScaled(scale_x,scale_y,scale_z);
/*	e3D_Object *Obj=this;

	int n=0, i;
	do
	{
		n++;
	} while((Obj=Obj->Parent)!=NULL);
	for (;n>0;n--)
	{
		Obj=this;

		for (i=1;i<n;i++) Obj=Obj->Parent;
		Obj->MultPosition();
	}
*/
}

void e3D_Object::GetXYZ(double *x, double *y,double *z)
{
	const dReal *p = dBodyGetPosition(body);
	*x=p[0];
	*y=p[1];
	*z=p[2];

	return;
	/*
  matriz M;

  glPushMatrix();
  LoadRealPosition();
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  matrix2point(M, x, y,z);
  glPopMatrix();*/
}

void e3D_Object::GetCamera(double dx,double dy, double dz)
{
  matriz M;
  double eyeX,eyeY,eyeZ;
  double centerX,centerY,centerZ;
  double upX,upY,upZ;

  glPushMatrix();
  LoadRealPosition();
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  matrix2point(M, &centerX, &centerY, &centerZ,0,0,-100000/camlag/camlag/camlag);
  glTranslated(dx,dy,dz);
  glGetDoublev(GL_MODELVIEW_MATRIX,M);
  matrix2point(M, &eyeX, &eyeY, &eyeZ);
  matrix2point(M, &upX, &upY, &upZ,0,10,0);

  upX-=eyeX;
  upY-=eyeY;
  upZ-=eyeZ;
	double camlag2=camlag/10.0;

  camera_eyeX=(camera_eyeX*camlag+eyeX)/(camlag+1);
  camera_eyeY=(camera_eyeY*camlag+eyeY)/(camlag+1);
  camera_eyeZ=(camera_eyeZ*camlag+eyeZ)/(camlag+1);

  camera_centerX=(camera_centerX*camlag2+centerX)/(camlag2+1);
  camera_centerY=(camera_centerY*camlag2+centerY)/(camlag2+1);
  camera_centerZ=(camera_centerZ*camlag2+centerZ)/(camlag2+1);

  camera_upX=(camera_upX*camlag+upX)/(camlag+1);
  camera_upY=(camera_upY*camlag+upY)/(camlag+1);
  camera_upZ=(camera_upZ*camlag+upZ)/(camlag+1);


  glPopMatrix();
    gluLookAt(camera_eyeX,camera_eyeY,camera_eyeZ,
            camera_centerX, camera_centerY, camera_centerZ,
            camera_upX, camera_upY, camera_upZ);

/*  gluLookAt(camera_eyeX,camera_eyeY,camera_eyeZ,
            camera_centerX, camera_centerY, camera_centerZ,
            camera_upX, camera_upY, camera_upZ);
*/
}

void e3D_Object::Translate(double x, double y, double z)
{
	double x1,y1,z1;
	GetXYZ(&x1,&y1,&z1);
	dBodySetPosition(body,x+x1,y+y1,z+z1);
/*
  glPushMatrix();
  LoadPosition();
  glTranslated(x,y,z);
  SavePosition();

//    if (Camera_follows_Translation)
//    {
    glLoadIdentity();
    glTranslated(-x,-y,-z);
    MultInverse();
    SaveInverse();
//    }
  glPopMatrix();
  */
}

void e3D_Object::Rotate(double angle,double x, double y, double z)
{
/*
  glPushMatrix();
  LoadPosition();
  glRotated(angle,x,y,z);
  SavePosition();

//    if (Camera_follows_Orientation)
//    {
    glLoadIdentity();
    glRotated(-angle,x,y,z);
    MultInverse();
    SaveInverse();
//    }
  glLoadIdentity();
  MultInverseO();
  glRotated(angle,x,y,z);
  SaveInverseO();

  glPopMatrix();
  */
}

void e3D_Object::Scale(double x, double y, double z)
{
	scale_x*=x;
	scale_y*=y;
	scale_z*=z;
}

void e3D_Object::BeginDraw()
{
  // Code to execute when at the beginning of a block of objects
  // Recommended: using glPus/PopAttrib prevents
  //      future problems when mixing render types
  glPushAttrib(GL_ALPHA_TEST | GL_POLYGON_SMOOTH | GL_TEXTURE_2D | GL_BLEND);
  /*glDisable(GL_ALPHA_TEST);
  glDisable(GL_BLEND);
  glEnable(GL_COLOR_MATERIAL);

  glDisable(GL_LIGHTING);*/

}

void e3D_Object::EndDraw()
{
  // Code to execute when at the beginning of a block of objects
  glPopAttrib();
}

void e3D_Object::Draw(int pass) // Example implementation for draw
{

	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
/*
  glColor4f(1.0f,1.0f,1.0f,1.0f);
  glBegin(GL_QUADS);
      glNormal3f( 0.0f, 0.0f, -1.0f);
      glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  -1.0f, 0.0f);
      glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  0.0f);
      glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  -1.0f, 0.0f);
  glEnd();
  */
}

e3D_Object *e3D_Object::DrawBE(bool AutoBeginEnd, e3D_Object *o_begin)
{

  // As a parameter, an e3D_Object indicates from what object started
  // drawing the loop. For Begin/End control purposes only.
  if (AutoBeginEnd)
  {
    if (o_begin)
    {
      if (o_begin->ObjectClassname!=ObjectClassname)
      {
        o_begin->EndDraw();
        o_begin=NULL;
      }
    }
    if (!o_begin)
    {
        o_begin=this;
        o_begin->BeginDraw();
    }
  }
  Draw();

  return o_begin;
}

e3D_Object *e3D_Object::LocateAndDraw(bool AutoBeginEnd, e3D_Object *o_begin)
{
  glPushMatrix();
  LoadRealPosition();
  o_begin=DrawBE(AutoBeginEnd,o_begin);
  glPopMatrix();

  return o_begin;
}

void e3D_Object::ApplyGravityFrom(e3D_Object *from)
{
	double Amass=mass.mass,Bmass=from->mass.mass;
	double Ax,Ay,Az;
	double Bx,By,Bz;
	GetXYZ(&Ax,&Ay,&Az);
	from->GetXYZ(&Bx,&By,&Bz);

	double ABx=Bx-Ax,ABy=By-Ay,ABz=Bz-Az;
	double Rxy=sqrt(ABx*ABx+ABy*ABy);
	double R2=Rxy*Rxy+ABz*ABz;
	double R=sqrt(R2);

	const double G=.0000001; // Universal Gravitation constant
	double N=G*Amass*Bmass/R2; // Gravity force
	//printf(" - %s - f: %.4f  a:%.4f b:%.1f r:%.1f \n", ObjectClassname.c_str(),N,Amass,Bmass,R);
	dBodyAddForceAtRelPos(body,ABx/R*N,ABy/R*N,ABz/R*N,0,0,-0.001);

}

void e3D_Object::ApplyGravityForces(e3D_Object *o_begin)
{
  // Locates and draws all childrens.
  //e3D_Object *obj=o_begin;

  e3D_Object *c;

  std::list<e3D_Object *>::iterator list_iter;
  for (list_iter = o_begin->children.begin(); list_iter != o_begin->children.end(); list_iter++)
  {
    c=*list_iter;
		ApplyGravityFrom(c);
  }
}

e3D_Object *e3D_Object::DrawChildren(
    bool AutoBeginEnd,
    bool draw_this,
    bool Recursive,
    e3D_Object *o_begin)
{
  // Locates and draws all childrens.
  e3D_Object *obj=o_begin;
  glPushMatrix();
  LoadRealPosition();
  if (draw_this)
  {
    obj=DrawBE(obj);
  }

  e3D_Object *c;

  std::list<e3D_Object *>::iterator list_iter;

  for (list_iter = children.begin(); list_iter != children.end(); list_iter++)
  {
    c=*list_iter;
//      c->Draw();
//    obj=c->LocateAndDraw(obj);
    if (Recursive)   obj=c->DrawChildren(AutoBeginEnd,true,true,obj);
      else  obj=c->LocateAndDraw(obj);

  }


  if (!o_begin && obj && AutoBeginEnd)
  {
    obj->EndDraw();
    obj=NULL;
  }

  glPopMatrix();

  return obj;
}


e3D_Ship::e3D_Ship(e3D_Object *parent, const char *fileobj, float size) : e3D_Object(parent)
{
	//e3D_Ship(parent);
	score=0;
	Ship.LoadObj(fileobj,size);
	_shield_size=shield_size=6;
	_bright_size=bright_size=0.1;
	if (__E3D_ENGINE_DEBUG) cout << "Create ship:" << fileobj << endl;
	init_physics();
	quad=gluNewQuadric();
	gluQuadricOrientation(quad, GLU_OUTSIDE);
	Shield=new e3D_Sprite(this,"images/escudos.png");
	Shield->Scale(shield_size,shield_size,shield_size);

	Bright=new e3D_Sprite(this,"images/particles/brightstar-256-128.png");
	Bright->Translate(0,0,100);
	Bright->Scale(bright_size,bright_size,bright_size);

	Bright2=new e3D_Sprite(Bright,"images/particles/hiddenplanet-256-128.png");
	Bright2->Translate(0,0,-4);
	Bright2->Scale(2,2,2);

	geom=dCreateSphere(Render->space,6);
	dMassSetSphere(&mass,10,6);
	dGeomSetBody(geom,body);
		//    Camera_follows_Orientation=false;
	rfriction=0.1;
	dfriction=0.1;
	dx=dy=dz=0;
	rx=ry=rz=0;
	hull=10;
	energy=0;
	maxenergy=10000;
	set_inc_energy=50;
	set_engine_energy=10;
	set_rotor_energy=10;
	used_fuel=0;
	time_start=Render->App->total_time;
	ObjectClassname="Ship";
}

void e3D_Ship::Draw(int pass) // Example implementation for draw
{
	const dReal *AngularVel, *LinearVel;

	AngularVel=dBodyGetAngularVel(body);
	LinearVel=dBodyGetLinearVel(body);

	double AngularVel_x=AngularVel[0],AngularVel_y=AngularVel[1],AngularVel_z=AngularVel[2];
	double LinearVel_x=LinearVel[0],LinearVel_y=LinearVel[1],LinearVel_z=LinearVel[2];


	lv=sqrt(LinearVel_x*LinearVel_x+LinearVel_y*LinearVel_y);
	lv=sqrt(lv*lv+LinearVel_z*LinearVel_z);

	if (rand()%5==0 && Render->Camera==this)
	{

		e3D_Shoot *shoot=new e3D_Shoot(
			Render->OG_Particles,"images/particles/supernova-256-16.png",
			0.1+log(lv+1),1);
		shoot->Translate(
			camera_eyeX+(rand()%200-100)/1.0,
			camera_eyeY+(rand()%200-100)/1.0,
			camera_eyeZ+(rand()%200-100)/1.0
		);
		shoot->Translate(
			LinearVel_x*5,
			LinearVel_y*5,
			LinearVel_z*5
		);
		dMassSetSphere(&(shoot->mass),1,4);
		shoot->life=7;
		shoot->speed=0;
		shoot->geom=0;
	}

	// Fricción simulada / irreal:
	const double avd=.500;
	dBodyAddTorque(body,-AngularVel_x*avd,-AngularVel_y*avd,-AngularVel_z*avd);
	const double lvd=.500;
	dBodyAddForce(body,-LinearVel_x*lvd,-LinearVel_y*lvd,-LinearVel_z*lvd);
	double lv2=sqrt(LinearVel_x*lvd*LinearVel_x*lvd+LinearVel_y*lvd*LinearVel_y*lvd);
	lv2=sqrt(lv2*lv2+LinearVel_z*lvd*LinearVel_z*lvd);

	// ******************** FISICA
	dBodyAddRelForce(body,-dx*(lvd+1),-dy*(lvd+1),-dz*(lvd+1)-lv2);
	dBodyAddRelTorque(body,-rx*(avd+1),-ry*(avd+1),-rz*(avd+1));
	float rxy=sqrt(rx*rx+ry*ry);
	float rxyz=sqrt(rz*rz+rxy*rxy);
	used_fuel+=rxyz/10.0;

	float dxy=sqrt(dx*dx+dy*dy);
	float dxyz=sqrt(dz*dz+dxy*dxy);
	used_fuel+=dxyz/10.0;

	//if (!pass || pass==PASS_SOLIDOBJECTS)
	energy+=2+_shield_size/(energy/set_inc_energy+1);
	/*energy-=fabs(dz)*set_engine_energy*dfriction;
	energy-=fabs(dx)*set_engine_energy*dfriction;
	energy-=fabs(dy)*set_engine_energy*dfriction;

	energy-=fabs(rz)*set_rotor_energy*rfriction;
	energy-=fabs(rx)*set_rotor_energy*rfriction;
	energy-=fabs(ry)*set_rotor_energy*rfriction;
	*/

	/*if (energy<=0)
	{
		if (bright_size<1)
		{
			dx/=2.0;
			dy/=2.0;
			dz/=2.0;

			rx/=2.0;
			ry/=2.0;
			rz/=2.0;
		}
		energy=0;
	}*/
	if (energy>maxenergy) energy=maxenergy;



	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	Ship.Draw();
	double _vel;
	_vel=sqrt(LinearVel_x*LinearVel_x+LinearVel_y*LinearVel_y);
	_vel=sqrt(_vel*_vel+LinearVel_z*LinearVel_z);
	/*
	bright_size=_vel;
	bright_size*=bright_size;
	bright_size-=1500;
	if (bright_size<0.01) bright_size=0.01;
	if (bright_size>100)  bright_size=100;*/
	/*Translate(-dx,-dy,-dz);
	Rotate(-rx,1,0,0);
	Rotate(-ry,0,1,0);
	Rotate(-rz,0,0,1);*/

	if (hull<=0)
	{
		dfriction=10;
		rfriction=0.1;
		hull=0;
		Scale(0.992,0.992,0.992);
		Shield->Scale(1.05,1.05,1.05);
		Bright->Scale(0.95,0.95,0.95);
		Bright2->Scale(1.01,1.01,1.01);
		shield_size=10;
		bright_size=100;
	}

	dx/=1+dfriction;
	dy/=1+dfriction;
	dz/=1+dfriction;

	rx/=1+rfriction+bright_size/100.0;
	ry/=1+rfriction+bright_size/100.0;
	rz/=1+rfriction+bright_size/100.0;

	ApplyGravityForces(Render->OG_Planets);
	// ***************************
	if (_bright_size<0.1) shield_size+=hull/(shield_size)/1000.0;
	if (_bright_size<0.1 && shield_size>50) shield_size/=1+(shield_size-15)/5000.0;
	if (shield_size>hull*2) shield_size/=1.001;
	if (shield_size<hull/5.0) shield_size=hull/5.0;
	if (shield_size!=_shield_size)
	{
		double rx=shield_size/_shield_size;
		rx-=1;
		rx/=10;
		rx+=1;

		Shield->Scale(rx,rx,rx);
		_shield_size*=rx;
	}
	if (bright_size!=_bright_size)
	{
		double rx=bright_size/_bright_size;
		Bright2->angle+=rx*rx-0.95;
		rx-=1;
		rx/=60;
		rx+=1;
		Bright->angle-=rx*rx-0.98;
		if (_bright_size>1)
		{
			if (rx<1) shield_size-=1-rx; else shield_size/=rx+.001;
		}
		Bright->Scale(rx,rx,rx);
		_bright_size*=rx;

	}

}


void e3D_Ship::Shoot(int type)
{
	e3D_Shoot *shoot;
	switch(type)
	{
		case 1:
			shoot=new e3D_Shoot(Render->OG_Particles,"images/particles/shoot1.png",1,15);
			shoot->ResetAtBody(this->body,0,0,-2-8-5);
			shoot->speed=20000;
			shoot->G=0.4;
			shoot->B=0.7;
			shoot->geom=dCreateSphere(Render->space,5);
			dGeomSetBody(shoot->geom,shoot->body);
			dMassSetSphere(&(shoot->mass),100,40);
			break;
		default:
		case 0:
			shoot=new e3D_Shoot(Render->OG_Particles,"images/particles/shoot1.png",1,.1);
			shoot->ResetAtBody(this->body,0,0,-1-5-1);
			shoot->speed=300;
			shoot->geom=dCreateSphere(Render->space,0.5);
			dGeomSetBody(shoot->geom,shoot->body);
			dMassSetSphere(&(shoot->mass),1,4);
			break;
	}

}

