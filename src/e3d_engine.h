// Libreria de clases del motor 3D.
#ifndef __E3D_ENGINE
#define __E3D_ENGINE
#include <stdio.h>
#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <ode/ode.h>
#ifndef APPLICATION_TITLE
#define APPLICATION_TITLE "NESPA2 v0.00"
#endif

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_timer.h"
#include "SDL/SDL_mixer.h"
#include "SDL_Pango.h"
#include "ModelType.h"

#define __E3D_ENGINE_DEBUG 0

#define MAX_SPRITE_LIMIT 512

#define PASS_BACKGROUND 1
#define PASS_SOLIDOBJECTS 2
#define PASS_PARTICLES 3

/** 
 * Resolución de fuentes: 100 - Mala; 300 - Aceptable; 600 - Buena
 */
#define TTF_DPI 100

using std::cout;
using std::string;
using std::list;
using std::endl;


class e3D_Video;
class e3D_Sound;
class e3D_Controller;
class e3D_Render;
class e3D_Object;
class e3D_Application;
class e3D_Sprite;
class e3D_Shoot;
class e3D_Ship;
class e3D_Text;
class e3D_Checkpoint;

// ******************************************************************
/**
 * @name Funciones auxiliares que operan directamente sobre las matrices.
/*@{*/
typedef double matriz[16];

void matrixidentity(matriz M);
void matrixresetrot(matriz M);
void matrix2angle(matriz M, double *x, double *y,double *z);
void matrix2point(matriz M, double *x, double *y,double *z,double px=0,double py=0,double pz=0);
/*@}*/

/**
 * Función para simplificar la carga de BMP's en SDL.
 */
SDL_Surface *LoadBMP(char *filename);





/** 
 * Clase de manipulación del modo de vídeo
 *
 * Se le especifica el modo deseado y se encarga de establecerlo y liberarlo adecuadamente.
 * 
 * Requiere:
 * Una clase e3D_Application que ya esté creada y funcionando.
 *
 * Modo de funcionamiento:
 * Crear la clase indicando en el constructor el e3D_Application. 
 * Establecer posteriormente Width, Height, Fullscreen y Bitsperpixel.
 * Lanzar las funciones InitializeVideo o Initialize según sea necesario. 
 * 
 * (se recomienda InitializeVideo)
 *
*/
class e3D_Video {
  public:
  e3D_Application *App; /**< Clase de Aplicación que controlará el modo de vídeo */

	bool Init;
  bool VideoInit;

  int Width; /**< Ancho en píxeles del modo de vídeo deseado */
  int Height; /**< Alto en píxeles del modo de vídeo deseado */
  int Bitsperpixel; /**< Profundidad de color en bits (4,8,16,24) */
  bool Fullscreen; /**< Flag de Pantalla completa. False para ventana */

  /*! 
	Constructor de la clase. Es inocuo.
	@param[in] app La clase de aplicación que controlará ésta clase
	*/
	e3D_Video(e3D_Application *app)
  {
    App=app;
    if (__E3D_ENGINE_DEBUG) printf("e3D_Video Constructed\n");
   // Initialize();
  }

  ~e3D_Video()
  {
    Destroy();
  }

  /*! 
	Función para incializar el vídeo. Es la que el programador debería ejecutar desde fuera. 
  Dado que comprueba si ya está el video inicializado, esta función puede ser usada a modo
	de "require" para evitar que ciertas operaciones gráficas se ejecuten sin entorno gráfico.
	
	No tiene argumentos, pero lee las variables Width, Height, Fullscreen (entre otras) para
	configurar adecuadamente el modo gráfico.
	*/
	int InitializeVideo();

	/*!
	Esta función sólo hace la parte inicial de la creación del modo de vídeo. No es habitual 
	llamarla directamente, pero puede ser útil hacerlo (en lugar de InitializeVideo) si lo que 
	necesitamos es únicamente acceso a algunas funciones de la SDL, ya ue después de ésta 
	función estarán disponibles.
	Al igual que InitializeVideo, comprueba si estaba ya inizializado o no de modo que se 
	puede usar para requerir el que SDL esté cargado.
	*/
  int Initialize()
  {
    if (Init) return 1;
    if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0 ) {
        fprintf(stderr, "e3D_Video::initialize() >> Unable to initialize SDL: %s\n", SDL_GetError());
        return 0;
    }
    if (__E3D_ENGINE_DEBUG) printf("Video preInitialized\n");
    Init=1;
    return 1;
  }
	
	/*! Esta función es el pseudo-destructor. Es la función que se encarga de descargar todos 
	los recursos que se hubieren cargado. Generalmente es llamada por el destructor 
	*/
  int Destroy()
  {
    if (!Init) return 1;
    SDL_Quit();
    if (__E3D_ENGINE_DEBUG) printf("Video Destroyed\n");
    Init=0;

    return 1;
  }

};


/**
 * Clase de inicialización del sistema de sonido. También proporciona una capa de 
 * abstracción (aunque pobre) para cargar y hacer sonar distitnos efectos sonoros y música.
 * 
 * Requiere:
 * Una clase e3D_Application previamente construida.
 *
 * Modo de funcionamiento:
 * Construir la clase pasando el e3D_Appllication y automáticamente el sistema de sonido
 * será inicializado. Al destruir se liberan los recursos automáticamente.
 * 
 * Adicionalmente se puede usar load_sound y play_sound para facilitar el uso de efectos.
 */
class e3D_Sound {
public:
  e3D_Application *App; /**< Aplicación que controla el sonido */
  int loaded; /**> Flag que indica si está iniciallizado el sonido */
  std::vector<Mix_Chunk*> SoundList; /**> Lista de sonidos cargados por load_sound */

	/*! 
		Constructor de e3D_Sound, inicializa directamente el sistema de audio.
	  \todo Agregar la posibilidad de cambiar la configuración.
	 	(22/44Khz, 16/24bits, 512/1024 samples de buffer)	 
	  \todo Agregar soporte para volúmenes configurables.
		
	 */
  e3D_Sound (e3D_Application *app)
  {
   App=app;
   if (Mix_OpenAudio(22050, AUDIO_S16, 2, 512) < 0)
//   if (Mix_OpenAudio(44100, AUDIO_S16, 2, 512) < 0)
//   if (Mix_OpenAudio(48000, AUDIO_S16, 2, 512) < 0)
    {
      fprintf(stderr, "Error: %s\n\n", SDL_GetError());
      loaded=0;
      return;
    }
    loaded=1;
    Mix_VolumeMusic(MIX_MAX_VOLUME/2);
  }

	/*!
		Carga un sonido y devuelve un identificador para poder llamarlo más tarde. 
		Tenga en cuenta que el sonido se carga en memoria, por lo que si carga demasiados
		sonidos saturará la memoria del equipo.
		@param[in] file String de c que indica el nombre de fichero a cargar.
		@return el número entero que identifica inequívocamente el sonido cargado.
	 */
	unsigned int load_sound(const char * file)
  // Load a sound file
  {
    if (!loaded) return NULL;

    Mix_Chunk * sound;
    sound = Mix_LoadWAV(file);
    if (!sound)
    {
      fprintf(stderr, "Error when loading sound %s: %s\n\n", file, SDL_GetError());
      return NULL;
    }
    SoundList.push_back(sound);
    //return(sound);
    return (SoundList.size() - 1);
  }
	
	/*!
		Hace sonar el efecto sonoro ID cargado previamente por load_sound, donde ID es el
		identificador que devolvió load_sound para el sonido deseado. Opcionalmente puede 
		especificar el volumen de 0 a 1 y la cantidad de bucles que desea que haga.
		Soporta los formatos WAV, MP3, OGG entre otros.
		@param[in] ID identificador de sonido devuelto por load_sound para el sonido deseado.
		@param[in] vol (Opcional) Volumen que desea para la reproducción. 0 es inaudible y 1 es el máximo posible. (valor por defecto: 1) 
		@param[in] loops (Opcional) Número de repeticiones que debe hacer el efecto antes de terminar. Cero es una repetición y uno son dos, y así consecutivamente. (valor por defecto: 0) (-1 si desea un bucle infinito)
		@return el identificador del canal por el que se reproduce el efecto sonoro. Esto le permite manipular los volúmenes mientras se reproduce.
	*/
	int play_sound(unsigned int ID, double vol=1, int loops=0)
	{
    if(ID < 0 || ID >= SoundList.size()) return -1;
    if(SoundList[ID] == NULL) return -1;
		int channel=Mix_PlayChannel(-1, SoundList[ID], loops);
		Mix_Volume(channel,MIX_MAX_VOLUME*vol);
    return channel;
	}

	/**!
		Carga la canción especificada en file. Devuelve el puntero hacia la estructura de datos
		de la canción cargada. Soporta tanto MIDI, MOD y otros formatos tracker, así como OGG, MP3 y otros formatos de onda.
	 */ 
  Mix_Music * load_music(char * file)
  {
    if (!loaded) return NULL;

    Mix_Music * music;
    music = Mix_LoadMUS(file);
    if (!music)
    {
      fprintf(stderr, "Error when loading music %s: %s\n\n", file, SDL_GetError());
      return NULL;
    }
    return(music);
  }

  bool play_music(Mix_Music * music, int loops)
  {
    if (music && loaded)
    {
      Mix_PlayMusic(music,loops);
      return 1;
    }
    return 0;
  }

  ~e3D_Sound ()
  {
    if (loaded)
    {
      loaded=0;
      Mix_HaltChannel(-1);
      Mix_CloseAudio();
    }
  }
};



class e3D_Controller {
public:
  e3D_Application *App;
  int warpx,warpy;
  int mouse_wheel;
  int mouse_x, mouse_y;
  bool grab;
  Uint8 *keys, mouseButtons;

  e3D_Controller(e3D_Application *app)
  {
   App=app;
  }
  void ReadMousePosAndWarp()
  {
    mouseButtons = SDL_GetRelativeMouseState(&mouse_x, &mouse_y);
    if (mouseButtons && !grab) Grab();
    if (grab)
    {
    SDL_WarpMouse(warpx,warpy);
    SDL_PumpEvents();
    SDL_GetRelativeMouseState(NULL,NULL);
    } else {mouse_x=mouse_y=0;}


  }
  void Grab()
  {
    grab=true;
    SDL_ShowCursor(0);
    SDL_WM_GrabInput(SDL_GRAB_ON);
  }
  void UnGrab()
  {
    grab=false;
    SDL_ShowCursor(1);
    SDL_WM_GrabInput(SDL_GRAB_OFF);
  }
  void SetWarpPos(int wx, int wy)
  {
    warpx=wx;
    warpy=wy;
    Grab();


    WarpMouse();
  }
  void WarpMouse()
  {
    if (grab)
    {
    SDL_WarpMouse(warpx,warpy);
    SDL_PumpEvents();
    SDL_GetRelativeMouseState(NULL,NULL);
    }
  }



  int GetEvents(bool updatemouse=true)
  {
    int done=0;
    int nkeys=0;
    mouse_wheel=0;

    SDL_Event event;
    while ( SDL_PollEvent(&event) ) {
        if ( event.type == SDL_QUIT ) done = 1;
        if ( event.type == SDL_KEYDOWN ) {
            if ( event.key.keysym.sym == SDLK_ESCAPE ) done = 1;
        }
        if ( event.type == SDL_MOUSEBUTTONDOWN) {
            if (event.button.button == SDL_BUTTON_WHEELDOWN) {
              mouse_wheel=1;
            }
            if (event.button.button == SDL_BUTTON_WHEELUP) {
              mouse_wheel=-1;
            }
        }
    }
    if (updatemouse) ReadMousePosAndWarp();
    keys = SDL_GetKeyState(&nkeys);

    return done;
  }



  ~e3D_Controller()
  {
  }
};

class e3D_Object {
  public:
  e3D_Object *Parent;
  e3D_Render *Render;
  list <e3D_Object *> children;

	dBodyID body;
	dGeomID geom;
	dMass mass;
/*  matriz position;
  matriz inverse;
  matriz inverse_orientation;
  */

	int AutoDeleteAfterDraw;
  string ObjectClassname;

//  bool Camera_follows_Translation;
//  bool Camera_follows_Orientation;
  //bool Camera_follows_Scale;
  double camera_eyeX,camera_eyeY,camera_eyeZ;
  double camera_centerX,camera_centerY,camera_centerZ;
  double camera_upX,camera_upY,camera_upZ;

	double scale_x;
	double scale_y;
	double scale_z;

	double camlag;

  e3D_Object(e3D_Render *render);
  e3D_Object(e3D_Object *parent);
  void init_physics(double size=1,double density=1);

  void ResetAtBody(dBodyID dest,double x, double y, double z, double vel_mult=1);

  virtual ~e3D_Object();

	void ApplyGravityFrom(e3D_Object *from);
	void ApplyGravityForces(e3D_Object *o_begin);

  void Translate(double x, double y, double z);
  void Rotate(double angle,double x, double y, double z);
  void Scale(double x, double y, double z);

  void Free();

  void BeginDraw();
  void EndDraw();
  void LoadRealPosition();
  /*
	void LoadPosition();
	void MultPosition();
	void SavePosition();

	void LoadInverse();
	void MultInverse();
	void SaveInverse();

	void LoadInverseO();
	void MultInverseO();
	void SaveInverseO();
*/

  double R,G,B,A;

  void GetCamera(double dx=0,double dy=0, double dz=0);
  void GetXYZ(double *x, double *y,double *z);

  virtual void Draw(int pass=0);

  virtual e3D_Object *DrawBE
            (bool AutoBeginEnd=false, e3D_Object *o_begin=NULL);
  virtual e3D_Object *LocateAndDraw
            (bool AutoBeginEnd=false, e3D_Object *o_begin=NULL);
  virtual e3D_Object *DrawChildren(
      bool AutoBeginEnd=true,
      bool draw_this=true,
      bool Recursive=true,
     e3D_Object *o_begin=NULL);


};



class e3D_Sprite : public e3D_Object {
  static string spritelist[MAX_SPRITE_LIMIT];
  static GLuint maxsprite;

  static GLuint sprite_shadow;
  public:
  bool shadowed;
  GLuint nsprite;
  double angle;
  e3D_Sprite(e3D_Object *parent, const char *sprite);
  e3D_Sprite(e3D_Object *parent, const char *sprite, double size, double density=1);
  virtual ~e3D_Sprite() {};
  void LoadSprite(const char *sprite);
  void LoadShadow(const char *spritefile);
  virtual void Draw(int pass);
};




class e3D_Text : public e3D_Object {
  public:
  SDLPango_Context *context;
  SDL_Surface *surface;
  GLuint sprite;
  int w,wz;
  int h,hz;
	int minX,minY;
	double x,y,z;
	float scaleX, scaleY;

  e3D_Text(e3D_Object *parent, const char *text=NULL) : e3D_Object(parent)
  {
  	context = SDLPango_CreateContext();
  	SDLPango_SetDefaultColor(context, MATRIX_TRANSPARENT_BACK_WHITE_LETTER);
    SDLPango_SetMinimumSize(context, 0, 0);
    SDLPango_SetDpi(context, TTF_DPI, TTF_DPI);
    surface=NULL;
	  glGenTextures(1, &sprite);
		glBindTexture(GL_TEXTURE_2D, sprite);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		x=y=z=minY=minX=0;
		scaleX=scaleY=10;
		setText("");
		R=G=B=A=1;

  }

  virtual ~e3D_Text() {SDLPango_FreeContext(context);};

	void setAttrs(int minX, int minY, float scaleX, float scaleY)
	{
		this->minX=minX;
		this->minY=minY;
    SDLPango_SetMinimumSize(context, minX, minY);
    SDLPango_SetDpi(context, TTF_DPI*scaleX, TTF_DPI*scaleY);
	}

	void setScale(float scaleX, float scaleY)
	{
		this->scaleX=scaleX;
		this->scaleY=scaleY;
	}

  void setText(const char *text)
  {
  	SDLPango_SetMarkup(context, text, -1);
    wz = w = SDLPango_GetLayoutWidth(context);
    hz = h = SDLPango_GetLayoutHeight(context);
		if (wz<minX) wz=minX;
		if (hz<minY) hz=minY;

  	if (surface)  SDL_FreeSurface(surface);
 		surface=SDL_CreateRGBSurface(SDL_SWSURFACE, wz+2, hz+2,
				32, (Uint32)(255 << (8 * 0)), (Uint32)(255 << (8 * 1)),
				(Uint32)(255 << (8 * 2)), (Uint32)(255 << (8 * 3)));
		SDLPango_Draw(context, surface, 1, 1);
		glBindTexture(GL_TEXTURE_2D, sprite);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA,
			surface->w, surface->h, GL_RGBA, GL_UNSIGNED_BYTE,
			surface->pixels);
  }

  void Draw(int pass)
  {

		glBindTexture(GL_TEXTURE_2D, sprite);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glDisable(GL_ALPHA_TEST);

		glBlendFunc(GL_SRC_ALPHA,GL_ONE); // Aditivo

		glPushMatrix();

/*		matriz M;
		glGetDoublev(GL_MODELVIEW_MATRIX,M);
		//matrixresetrot(M);
		glLoadMatrixd(M);
//		glRotated(angle,0,0,1);
		//Render->LoadCameraRot(Render->Camera);
		*/
    glTranslatef(x,y,z);
		glScaled(wz/(TTF_DPI*scaleX),hz/(TTF_DPI*scaleY),0.0);
		glColor4f(R,G,B,A);
		glBegin(GL_QUADS);
			glNormal3f( 0.0f, 0.0f, -1.0f);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f, 0.0f);
			glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f, 0.0f);
			glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  -1.0f,  0.0f);
			glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  -1.0f,  0.0f);
		glEnd();
		glPopMatrix();

  }
};

class e3D_Shoot : public e3D_Sprite {
	public:

	float life;
	float speed;
	float max_size;
  e3D_Shoot(e3D_Object *parent, const char *sprite, double size, double density=1) :
  	e3D_Sprite(parent, sprite, size, density)
	{
		scale_x=.05;
		scale_y=.05;
		scale_z=.05;
		life=60;
		max_size=size;
		ObjectClassname="Shoot";
	}
	e3D_Ship *parent_ship;

  void Draw(int pass);
};


class e3D_Ship : public e3D_Object {
  public:
  ModelType Ship;
  GLUquadric*    quad;
  double shield_size, _shield_size;
  double bright_size, _bright_size;
  double hull;

  double  dx,dy,dz, dfriction;
  double  rx,ry,rz, rfriction;
	double lv;

  bool shoot;

  double energy, maxenergy;

  double used_fuel;

  double set_inc_energy;
  double set_engine_energy;
  double set_rotor_energy;

	int score;
	double time_start;

  e3D_Object *Target;

  e3D_Sprite *Shield;
  e3D_Sprite *Bright;
  e3D_Sprite *Bright2;

  e3D_Ship(e3D_Object *parent, const char *fileobj, float size);
  virtual ~e3D_Ship() {};
	void Shoot(int type);
  virtual void Draw(int pass=0); // Example implementation for draw

};


class e3D_Render {
  public:
  e3D_Application *App;
  bool InitGL;
  double aspect_ratio;
  float zoom,d_size;

  e3D_Object *Camera;
  e3D_Object *World;

  e3D_Object *OG_Particles;
  e3D_Object *OG_Ships;
  e3D_Object *OG_Planets;
  e3D_Object *OG_Stars;
  e3D_Object *OG_HUD;

  dWorldID world;
	dSpaceID space;
	dJointGroupID _contactgroup;

  e3D_Ship *Ship1;
  e3D_Ship *Ship2;
  e3D_Ship *Ship3;

  e3D_Sprite *Sun;
  e3D_Sprite *Blackhole;
  e3D_Sprite *Planet1;


  e3D_Text *Time;
  e3D_Text *Score;
  e3D_Text *Speed;
  e3D_Text *Fuel;

  e3D_Ship *Player;
/*  e3D_Object *Ships;
  e3D_Object *Planets;
  e3D_Object *Particles;*/


  e3D_Render(e3D_Application *app)
  {
    App=app;
    InitGL=false;
    world = dWorldCreate();
		space = dHashSpaceCreate((dSpaceID) 0);
		_contactgroup = dJointGroupCreate(0);

    World=new e3D_Object(this);
  }

  ~e3D_Render()
  {
    if (World) delete World;
  }
	void Init(int Width,int Height);

  void LoadCamera(e3D_Object *camera=NULL)
  {
/*    if (!camera) camera=Camera;
    if (!camera) return;
    do
    {
      camera->MultInverse();
    } while((camera=camera->Parent)!=NULL);*/
  }

  void LoadCameraRot(e3D_Object *camera=NULL)
  {
/*    if (!camera) camera=Camera;
    if (!camera) return;
    do
    {
      camera->MultInverseO();
    } while((camera=camera->Parent)!=NULL);*/
  }

  void Render()
  {

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(30.0f/zoom,aspect_ratio,0.1f,10000000.0f);
//    gluPerspective(30.0f/zoom,aspect_ratio,0.1f*zoom,10000000.0f*zoom);

    glMatrixMode(GL_MODELVIEW);


    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0);
    glDepthMask(GL_TRUE);
    glLoadIdentity();
    //glScaled(1.0/zoom,1.0/zoom,1.0/zoom);

    Camera->GetCamera(0,sqrt(10/zoom)-0.5,30/zoom-10);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Pass3_ParticlesAndAtmospheres();
    Pass2_Ships();
    Pass1_StarsAndPlanets();
//    LoadCamera(Camera);
  // Setup Interface
  // Render Interface
		e3D_Object *c;
		std::list<e3D_Object *>::iterator list_iter;

		for (list_iter = OG_Particles->children.begin(); list_iter != OG_Particles->children.end(); list_iter++)
		{
			c=*list_iter;
			if (c->AutoDeleteAfterDraw==1) { delete c; break;}
		}
  }

  void Pass3_ParticlesAndAtmospheres()
  {
    // Draw Particles
		OG_Particles->DrawChildren();

    // Draw Planet Atmospheres.
  }


  void Pass2_Ships()
  {
      OG_Ships->DrawChildren();
  }

  void Pass1_StarsAndPlanets()
  {
      // Draw stars.

      OG_Stars->DrawChildren();
      OG_Planets->DrawChildren();

      // Draw Planets
  }


};

// **********************************************************

// Clase de manejo de la aplicacion.
class e3D_Application {

  public:
  Uint32 time_start, time_elapsed, total_time;
  Uint32 frame;
  Uint32 miliseconds_per_frame;

  e3D_Video *Video;
  e3D_Sound *Sound;
  e3D_Controller *Controller;
  e3D_Render *Render;

  double qcpu;

  void SetFPS(int fps)
  {
    miliseconds_per_frame=1000/fps;
  }

  bool StartFrame()
  {
    bool should_draw=time_elapsed<miliseconds_per_frame*2;

    frame++;


    time_elapsed=SDL_GetTicks()-time_start;
    if (time_elapsed<miliseconds_per_frame)
    {
      SDL_Delay(miliseconds_per_frame-time_elapsed);
    }
    qcpu=(qcpu*100+time_elapsed/(double)miliseconds_per_frame)/101;
    time_start=SDL_GetTicks();
    total_time+=time_elapsed;

    return should_draw;

  }

  e3D_Application();
  ~e3D_Application();



};


class e3D_Checkpoint : public e3D_Sprite {
  static e3D_Checkpoint *checkpointlist[128];
  static int maxcheckpoint;
  static int currentpoint;
  public:
	double planet_size;
	int cpoint;
	int total_score;
  virtual void Draw(int pass);
	e3D_Checkpoint(e3D_Object *parent);
  virtual ~e3D_Checkpoint() {};

};


#endif
