#!/bin/sh

MIRROR="http://www.sedice.com/otraswebs/nespa/music/"

mkdir music 2>/dev/null
cd music
for music in `cat ../music_packs.txt`; do
	wget $MIRROR$music
	tar xf $music
	unlink $music
done
