 - <<Tareas a realizar.

 - Utilización del comando glut para situación de cámara.
void gluLookAt( GLdouble    eyeX, 
  GLdouble    eyeY, 
  GLdouble    eyeZ, 
  GLdouble    centerX, 
  GLdouble    centerY, 
  GLdouble    centerZ, 
  GLdouble    upX, 
  GLdouble    upY, 
  GLdouble    upZ);


  eyeX,y,z es la posición de la nave, + desplazamiento de la cámara debido a juego en 3ª persona.

  centerX,Y,Z es el punto donde debe mirar la cámara: nave - punto_visión + cámara

  upX,Y,Z es el vector vertical de la cámara. En principio es el mismo que el de la nave.

  Cosas que se conocen:

  - La matriz 4x4 de la nave.

  
  